FROM maven:3-openjdk-17-slim AS build
WORKDIR /app/build
COPY . .
RUN mvn package -B

FROM openjdk:17-slim
WORKDIR /app/bin
COPY --from=build /app/build/target/app.jar .
CMD ["java", "-jar", "app.jar"]